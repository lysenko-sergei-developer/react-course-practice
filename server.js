let express = require('express')
require("babel-core").transform("code", {
  plugins: ["transform-react-jsx"]
});

let app = express()

app.set('port', (process.env.PORT || 3000))

app.use('/', express.static(__dirname))

app.listen(app.get('port'), function(){
  console.log('Server started:' + app.get('port'))
})
