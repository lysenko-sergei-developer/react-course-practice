'use strict';

let my_news = [
  {
    author: 'Саша Печкин',
    text: 'В четчерг, четвертого числа...',
    bigText: 'в четыре с четвертью часа четыре чёрненьких чумазеньких чертёнка чертили чёрными чернилами чертёж.'
  },
  {
    author: 'Просто Вася',
    text: 'Считаю, что $ должен стоить 35 рублей!',
    bigText: 'А евро 42!'
  },
  {
    author: 'Гость',
    text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
    bigText: 'На самом деле платно, просто нужно прочитать очень длинное лицензионное соглашение'
  }
]

window.ee = new EventEmitter();

let Article = React.createClass({
  propTypes: {
    data: React.PropTypes.shape({
      author: React.PropTypes.string.isRequired,
      text: React.PropTypes.string.isRequired,
      bigText: React.PropTypes.string.isRequired,
    })
  },
  getInitialState: function() {
    return {
      visible: false
    }
  },
  readmoreClick: function(e) {
    e.preventDefault()
    this.setState({visible: true})
  },
  render: function() {
    let author = this.props.data.author,
        text = this.props.data.text,
        bigText = this.props.data.bigText,
        visible = this.state.visible
    return (
      <div className="article">
        <p className="news__author">{author}:</p>
        <p className="news__text">{text}:</p>
        <a href="#" onClick={this.readmoreClick} className={"news__readmore " + (visible ? 'none' : '')}>Подробнее</a>
        <p className={"news__big-text " + (visible ? '' : 'none')}>{bigText}</p>
      </div>
    )
  }
})

let News = React.createClass({
  propTypes: {
    data: React.PropTypes.array.isRequired
  },
  getInitialState: function() {
    return {
      counter: 0
    }
  },
  render: function() {
    let data = this.props.data
    let newsTemplate

    if (data.length > 0) {
      newsTemplate = data.map(function(item, index) {
        return (
          <div key={index}>
            <Article data={item}/>
          </div>
        )
      })
    } else {
      newsTemplate = <p>К сожалению новостей нет</p>
    }
      return (
        <div className="news">
          {newsTemplate}
          <strong className={'news__count ' + (data.length > 0 ? '' : 'none')}>
           Всего новостей: {data.length}
         </strong>
        </div>
      )
  }
})

let Add = React.createClass({
  getInitialState: function() {
    return {
      btnIsDisabled: true,
      authorIsEmpty: true,
      textIsEmpty: true
    }
  },
  componentDidMount: function() {
    ReactDOM.findDOMNode(this.refs.author).focus()
  },
  onFieldChange: function(fieldsName, e) {
    if(e.target.value.trim().length > 0) {
      this.setState({[''+fieldsName]: false})
    } else {
      this.setState({[''+fieldsName]: true})
    }
  },
  onCheckRuleClick: function(e) {
    // ReactDOM.findDOMNode(this.refs.alert_button).disabled = !e.target.checked;
    this.setState({
      btnIsDisabled: !this.state.btnIsDisabled
    })
  },
  onClickButton: function(e) {
    e.preventDefault()
    let textEl = ReactDOM.findDOMNode(this.refs.text)

    let author = ReactDOM.findDOMNode(this.refs.author).value
    let text = textEl.value

    var item = [{
      author: author,
      text: text,
      bigText: '...'
    }]

    // alert(author + '\n' + text)
    window.ee.emit('News.add', item)

    textEl.value = ''
    this.setState({textIsEmpty: true})
  },
  render: function() {
    return (
      <form className='add cf'>
        <input
          className='test-input'
          defaultValue=''
          onChange={this.onFieldChange.bind(this, 'authorIsEmpty')}
          placeholder='введите значение'
          ref='author'
        />
        <textarea
          className='add_text'
          defaultValue=''
          onChange={this.onFieldChange.bind(this, 'textIsEmpty')}
          placeholder='Текст новости'
          ref='text'
        ></textarea>
        <label className='add_checkrule'>
          <input
            type='checkbox'
            ref='checkrule'
            onChange={this.onCheckRuleClick}
          />я согласен с правилами
        </label>
        <button
          onClick={this.onClickButton}
          ref='alert_button'
          className='add_btn'
          disabled={this.state.btnIsDisabled || this.state.authorIsEmpty || this.state.textIsEmpty}>
          Добавить новость!
        </button>
      </form>
    )
  }
})

let App = React.createClass({
  getInitialState: function(e) {
    return {
      news: my_news
    }
  },
  componentDidMount: function() {
    var self = this
    window.ee.addListener('News.add', function(item) {
      var nextNews = item.concat(self.state.news)
      self.setState({news: nextNews})
    });
  },
  componentWillUnmount: function() {
    window.ee.removeListener('News.add')
  },
  render: function() {
    return (
      <div className="app">
        <Add />
        <h3> Новости </h3>
        <News data={this.state.news}/> {/* Вот такой длинный коментарий*/}
      </div>
    )
  }
})

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
