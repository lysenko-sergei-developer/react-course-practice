'use strict';

let my_news = [{
  author: 'Саша Печкин',
  text: 'В четчерг, четвертого числа...',
  bigText: 'в четыре с четвертью часа четыре чёрненьких чумазеньких чертёнка чертили чёрными чернилами чертёж.'
}, {
  author: 'Просто Вася',
  text: 'Считаю, что $ должен стоить 35 рублей!',
  bigText: 'А евро 42!'
}, {
  author: 'Гость',
  text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
  bigText: 'На самом деле платно, просто нужно прочитать очень длинное лицензионное соглашение'
}];

window.ee = new EventEmitter();

let Article = React.createClass({
  propTypes: {
    data: React.PropTypes.shape({
      author: React.PropTypes.string.isRequired,
      text: React.PropTypes.string.isRequired,
      bigText: React.PropTypes.string.isRequired
    })
  },
  getInitialState: function () {
    return {
      visible: false
    };
  },
  readmoreClick: function (e) {
    e.preventDefault();
    this.setState({ visible: true });
  },
  render: function () {
    let author = this.props.data.author,
        text = this.props.data.text,
        bigText = this.props.data.bigText,
        visible = this.state.visible;
    return React.createElement(
      'div',
      { className: 'article' },
      React.createElement(
        'p',
        { className: 'news__author' },
        author,
        ':'
      ),
      React.createElement(
        'p',
        { className: 'news__text' },
        text,
        ':'
      ),
      React.createElement(
        'a',
        { href: '#', onClick: this.readmoreClick, className: "news__readmore " + (visible ? 'none' : '') },
        '\u041F\u043E\u0434\u0440\u043E\u0431\u043D\u0435\u0435'
      ),
      React.createElement(
        'p',
        { className: "news__big-text " + (visible ? '' : 'none') },
        bigText
      )
    );
  }
});

let News = React.createClass({
  propTypes: {
    data: React.PropTypes.array.isRequired
  },
  getInitialState: function () {
    return {
      counter: 0
    };
  },
  render: function () {
    let data = this.props.data;
    let newsTemplate;

    if (data.length > 0) {
      newsTemplate = data.map(function (item, index) {
        return React.createElement(
          'div',
          { key: index },
          React.createElement(Article, { data: item })
        );
      });
    } else {
      newsTemplate = React.createElement(
        'p',
        null,
        '\u041A \u0441\u043E\u0436\u0430\u043B\u0435\u043D\u0438\u044E \u043D\u043E\u0432\u043E\u0441\u0442\u0435\u0439 \u043D\u0435\u0442'
      );
    }
    return React.createElement(
      'div',
      { className: 'news' },
      newsTemplate,
      React.createElement(
        'strong',
        { className: 'news__count ' + (data.length > 0 ? '' : 'none') },
        '\u0412\u0441\u0435\u0433\u043E \u043D\u043E\u0432\u043E\u0441\u0442\u0435\u0439: ',
        data.length
      )
    );
  }
});

let Add = React.createClass({
  getInitialState: function () {
    return {
      btnIsDisabled: true,
      authorIsEmpty: true,
      textIsEmpty: true
    };
  },
  componentDidMount: function () {
    ReactDOM.findDOMNode(this.refs.author).focus();
  },
  onFieldChange: function (fieldsName, e) {
    if (e.target.value.trim().length > 0) {
      this.setState({ ['' + fieldsName]: false });
    } else {
      this.setState({ ['' + fieldsName]: true });
    }
  },
  onCheckRuleClick: function (e) {
    // ReactDOM.findDOMNode(this.refs.alert_button).disabled = !e.target.checked;
    this.setState({
      btnIsDisabled: !this.state.btnIsDisabled
    });
  },
  onClickButton: function (e) {
    e.preventDefault();
    let textEl = ReactDOM.findDOMNode(this.refs.text);

    let author = ReactDOM.findDOMNode(this.refs.author).value;
    let text = textEl.value;

    var item = [{
      author: author,
      text: text,
      bigText: '...'
    }];

    // alert(author + '\n' + text)
    window.ee.emit('News.add', item);

    textEl.value = '';
    this.setState({ textIsEmpty: true });
  },
  render: function () {
    return React.createElement(
      'form',
      { className: 'add cf' },
      React.createElement('input', {
        className: 'test-input',
        defaultValue: '',
        onChange: this.onFieldChange.bind(this, 'authorIsEmpty'),
        placeholder: '\u0432\u0432\u0435\u0434\u0438\u0442\u0435 \u0437\u043D\u0430\u0447\u0435\u043D\u0438\u0435',
        ref: 'author'
      }),
      React.createElement('textarea', {
        className: 'add_text',
        defaultValue: '',
        onChange: this.onFieldChange.bind(this, 'textIsEmpty'),
        placeholder: '\u0422\u0435\u043A\u0441\u0442 \u043D\u043E\u0432\u043E\u0441\u0442\u0438',
        ref: 'text'
      }),
      React.createElement(
        'label',
        { className: 'add_checkrule' },
        React.createElement('input', {
          type: 'checkbox',
          ref: 'checkrule',
          onChange: this.onCheckRuleClick
        }),
        '\u044F \u0441\u043E\u0433\u043B\u0430\u0441\u0435\u043D \u0441 \u043F\u0440\u0430\u0432\u0438\u043B\u0430\u043C\u0438'
      ),
      React.createElement(
        'button',
        {
          onClick: this.onClickButton,
          ref: 'alert_button',
          className: 'add_btn',
          disabled: this.state.btnIsDisabled || this.state.authorIsEmpty || this.state.textIsEmpty },
        '\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u043D\u043E\u0432\u043E\u0441\u0442\u044C!'
      )
    );
  }
});

let App = React.createClass({
  getInitialState: function (e) {
    return {
      news: my_news
    };
  },
  componentDidMount: function () {
    var self = this;
    window.ee.addListener('News.add', function (item) {
      var nextNews = item.concat(self.state.news);
      self.setState({ news: nextNews });
    });
  },
  componentWillUnmount: function () {
    window.ee.removeListener('News.add');
  },
  render: function () {
    return React.createElement(
      'div',
      { className: 'app' },
      React.createElement(Add, null),
      React.createElement(
        'h3',
        null,
        ' \u041D\u043E\u0432\u043E\u0441\u0442\u0438 '
      ),
      React.createElement(News, { data: this.state.news }),
      ' '
    );
  }
});

ReactDOM.render(React.createElement(App, null), document.getElementById('root'));
